# `yt-invidious`

Get a link to a Youtube video filtered by a random [**Invidious**](https://invidious.io/) instance.

Usage (examples):
```bash
# get a list of available regions
yt-inv.py

# get a random link choosing among all available instances
yt-inv.py https://www.youtube.com/watch?v=fDjyHxKIfyE

# get a random link choosing among all available instances from Germany
yt-inv.py https://www.youtube.com/watch?v=fDjyHxKIfyE de
```