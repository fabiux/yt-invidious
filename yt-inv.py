#!/usr/bin/env python
"""
Just an example on how to deal with Invidious active instances feed
in order to randomly choose one.

Usage:
yt-inv.py <YT-VIDEO-URL> [REGION]

yt-inv.py with no arguments to print a list of available regions
"""
from sys import argv
from os.path import isfile, getctime
from datetime import datetime
import requests
from random import choice
from urllib.parse import urlparse

cache_file = '/var/tmp/invidious.json'
cache_lifetime = 86400  # secs
url = 'https://api.invidious.io/instances.json?sort_by=users'

c_time = 0
if isfile(cache_file):
    c_time = int(getctime(cache_file))

if (int(datetime.now().strftime('%s')) - c_time) > cache_lifetime:
    r = requests.get(url)
    if r.status_code >= 300:
        exit('error retrieving feed')
    with open(cache_file, 'w') as f:
        f.write(repr(r.json()))

with open(cache_file, 'r') as f:
    j = eval(f.read())

if len(argv) == 1:  # print a list of available regions
    print('Usage: yt-inv.py <YT-VIDEO-URL> [REGION]')
    print('\nList of available regions:')
    regions = []
    for row in j:
        if row[1]['type'] == 'https':  # https only - you may want to get all instances here
            regions.append(row[1]['region'])
    exit(repr(sorted(list(set(regions)))))

region = argv[2].upper() if len(argv) > 2 else ''
instances = []
for row in j:
    if row[1]['type'] == 'https':
        if region and (region != row[1]['region']):
            continue
        instances.append(row[1]['uri'])

p = urlparse(argv[1])
print('{}{}?{}'.format(choice(instances), p.path, p.query))
